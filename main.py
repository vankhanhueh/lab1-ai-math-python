import glob
import networkx as nx
import matplotlib.pyplot as plt

## Lớp vẽ đồ thị
class GraphVisualization:
    def __init__(self):
        self.visual = []

    def addEdge(self, a, b):
        temp = [a, b]
        self.visual.append(temp)
          
    def visualize(self):
        G = nx.Graph()
        G.add_edges_from(self.visual)
        nx.draw(G, with_labels=True, node_size=1000, font_size=9, font_color="yellow")
        plt.show()
  
## Hàm kiểm tra xem node a và Node b có nối được hay không
def checkNodeToNode(a, b):
    if(a == b): return False
    isDifferent = 0
    for x in range(len(a)):
        if(a[x] != b[x]): isDifferent = isDifferent + 1
        if(isDifferent>= 2): return False
    return True

##Hàm biểu diễn đồ thị
##dataFilePath: đường dẫn của data đầu vào
##isLoadData: có load lại data từ thư mục data hay không
def createGraph(dataFilePath, isLoadData):
    G = GraphVisualization()
    if(isLoadData):
        fLoad = open(dataFilePath, "r")
        data = fLoad.read().split("\n")
        for item in data:
            nodes = []
            for item1 in data:
                if(checkNodeToNode(item, item1)):
                    nodes.append(item1)
            fWrite = open("data/" + item, "w+")
            fWrite.write("\n".join(nodes))
            fWrite.close()
    allNodes = glob.glob("data/*")
    for node in allNodes:
        fLoad = open(node, "r")
        leafs = fLoad.read().split("\n")
        fName = node.split("/")[-1]
        print(fName)
        for leaf in leafs:
            G.addEdge(fName, leaf)
    G.visualize() 

##Tạo đồ thị từ dữ liệu đầu vào
createGraph("DataTest1.txt", False)